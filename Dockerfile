FROM phusion/passenger-full:1.0.9

ENV HOME /root

CMD ["/sbin/my_init"]

RUN apt-get update 
RUN apt-get install sudo
RUN apt-get install -y curl
RUN sudo apt install -y git
RUN sudo apt-get install python3
RUN sudo apt install -y python3-pip
RUN sudo pip3 install flask
RUN sudo pip3 install Flask-MySQLdb
#RUN sudo apt-get install rsyslog -y 

RUN rm -f /etc/service/nginx/down
RUN rm /etc/nginx/sites-enabled/default

RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.10.0-amd64.deb
RUN sudo dpkg -i filebeat-7.10.0-amd64.deb

COPY ./filebeat.yml /etc/filebeat/filebeat.yml

RUN sudo filebeat modules enable system
RUN sudo filebeat modules enable nginx

RUN curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.10.0-amd64.deb 
RUN sudo dpkg -i metricbeat-7.10.0-amd64.deb

COPY ./metricbeat.yml /etc/metricbeat/metricbeat.yml

RUN sudo metricbeat modules enable system
RUN sudo metricbeat modules enable nginx


RUN curl -L -O https://artifacts.elastic.co/downloads/apm-server/apm-server-7.10.0-amd64.deb
RUN sudo dpkg -i apm-server-7.10.0-amd64.deb

COPY ./apm-server.yml /etc/apm-server/apm-server.yml


COPY ./webapp.conf /etc/nginx/sites-enabled/webapp.conf

RUN mkdir /home/app/webapp
COPY --chown=app:app ./code /home/app/webapp

COPY ./setup_conf.sh /
RUN chmod +x /setup_conf.sh
ENTRYPOINT ["/setup_conf.sh"]

WORKDIR /home/app/webapp
RUN sudo pip3 install -r requirements.txt

RUN pip3 install -e .

RUN sudo service filebeat start
RUN sudo service metricbeat start
RUN sudo service apm-server start


RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
