#!/bin/bash
set -e
PLACEHOLDER_VHOST="$(curl http://169.254.169.254/latest/meta-data/public-hostname)"
DEFAULT_CONFIG_PATH="/etc/nginx/sites-enabled/webapp.conf"
DEFAULT_INSTANCE_PATH="/home/app/webapp/app_config/config.py"
sed -i "s/PLACEHOLDER_VHOST/${PLACEHOLDER_VHOST}/g" "${DEFAULT_CONFIG_PATH}"
sed -i "s/PLACEHOLDER_VHOST/${PLACEHOLDER_VHOST}/g" "${DEFAULT_INSTANCE_PATH}"
exec "$@"