from flask import Blueprint, render_template, request,redirect,url_for

from flask_login import login_required,login_user,current_user,logout_user

from project_services.track.forms import TrackForm
from project_services.track.model import Track

track_blueprint = Blueprint('track', __name__, template_folder='templates')


import logging
from app_library import lib_logger as logger

#@track_blueprint.route('/track',methods = ['GET'])
#@login_required
#def track():
#    return render_template('track/track.html')


@track_blueprint.route('/track',methods = ['GET','POST'])
@login_required
def track():

    form = TrackForm()

    if form.validate_on_submit():

        t = Track()
        #ta = Track()

        #ta.t_id = form.t_id.data
        #ta.t_lat = form.t_lat.data
        #ta.t_lon = form.t_lon.data

        form.populate_obj(t)
        t.save()

        #print(ta)
        #print(ta.t_id)
        #print(ta.t_lat)
        #print(ta.t_lon)

        #logger.log("false","true",t)
        #return redirect(url_for('track.result',data=ta))
        return render_template('track/result.html', result=t)

    return render_template("track/track.html",form=form)

@track_blueprint.route('/result/<data>')
@login_required
def result(data):
    # Doce did not work :(
    #logger.log("false","true","/result/<data>" + data
    #print(data)
    #print(data.t_id)
    #print(data.t_lat)
    #print(data.t_lon)

    return render_template('track/result.html', result=data)



