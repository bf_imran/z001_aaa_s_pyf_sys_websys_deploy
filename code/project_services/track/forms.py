from flask_wtf import Form
from wtforms import HiddenField, StringField, PasswordField
from wtforms.validators import DataRequired, Length

from app_library.lib_wtforms import ModelForm
from project_services.user.models import User, db
from project_services.user.validations import ensure_identity_exists, ensure_existing_password_matches

class TrackForm(Form):
    t_id = StringField('t_id', [DataRequired(), Length(1, 254)])
    t_lat = StringField('t_lat', [DataRequired(), Length(1, 254)])
    t_lon = StringField('t_lon', [DataRequired(), Length(1, 254)])