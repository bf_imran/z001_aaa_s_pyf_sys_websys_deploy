from flask import Blueprint, render_template, request,redirect
from flask import Response, json
from flask import jsonify
from flask import current_app

import logging
from app_library import lib_logger as logger
from server_app_extensions import apm

health_blueprint = Blueprint('health', __name__, template_folder='templates')

@health_blueprint.route('/health-report')
def check_health():
    return render_template('health/report_health.html')

@health_blueprint.route('/health', methods=["GET"])
def get_health():
    jd = {'health status': 'OK'}
    data = json.dumps(jd)
    resp = Response(data, status=200, mimetype='application/json')
    return resp


@health_blueprint.route("/get_ip", methods=["GET"])
def get_my_ip():
    return jsonify({'ip': request.remote_addr}), 200


@health_blueprint.route('/log')
def trigger_logger():

    log_file = current_app.config["LOG_TO_FILE"]
    log_console = current_app.config["LOG_TO_CONSOLE"]
    logger.log(log_file,log_console,"Logger trggered from /log")
    jd = {'log status': 'OK'}
    data = json.dumps(jd)
    resp = Response(data, status=200, mimetype='application/json')
    return resp
    
    
@health_blueprint.route('/log-apm')
def trigger_apm_logger():

    log_file = current_app.config["LOG_TO_FILE"]
    log_console = current_app.config["LOG_TO_CONSOLE"]
    logger.log(log_file,log_console,"Logger trggered from /log-apm")
    apm.capture_message('Triggered apm capture from flask app.')

    jd = {'log apm status': 'OK'}
    data = json.dumps(jd)
    resp = Response(data, status=200, mimetype='application/json')
    return resp
    
 
@health_blueprint.route('/log-apm-error')
def trigger_apm_exception():

    log_file = current_app.config["LOG_TO_FILE"]
    log_console = current_app.config["LOG_TO_CONSOLE"]
    logger.log(log_file,log_console,"Logger trggered from /log-apm-error")
    apm.capture_message('Triggered error and catching via apm capture exception from flask app.')

    try:
        1 / 0
    except ZeroDivisionError:
        apm.capture_exception()
        
    return redirect("http://" + current_app.config["SERVER_NAME"])