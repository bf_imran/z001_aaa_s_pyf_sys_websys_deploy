import logging
import time
from time import localtime, strftime

formatter = logging.Formatter('%(asctime)s')
formatter.default_msec_format = '%s.%03d'


def setuplog(log_file, log_console, path):
    if log_file:
        logging.basicConfig(filename=strftime(path, localtime()), level=logging.DEBUG,
                            format='%(asctime)s : %(levelname)s : %(name)s : %(threadName)s : %(message)s', datefmt='%Y-%m-%dT%H:%M:%S%z')
        #logging.basicConfig(filename=strftime(path, localtime()),level=logging.DEBUG,format='%(asctime)s.%(msecs)03d : %(levelname)s : %(name)s : %(threadName)s : %(message)s', datefmt='%Y-%m-%dT%H:%M:%S.%f+%z')
        #logging.basicConfig(filename=strftime(path, localtime()),level=logging.DEBUG,format='%(asctime)s : %(levelname)s : %(name)s : %(threadName)s : %(message)s', datefmt='%Y-%m-%dT%I:%M:%S %p %z')
        # Ref > https://docs.python.org/3/library/logging.html#logging.Formatter.formatTime


def log(log_file, log_console, message, severity=logging.DEBUG):
    if log_console:
        print(message)
    if log_file:
        logging.debug(message)


def stopmsg(log_file, log_console):
    message = "Server Stopped."
    if log_console:
        print(message)
    if log_file:
        logging.debug(message)

