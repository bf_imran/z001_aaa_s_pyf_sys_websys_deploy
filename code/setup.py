from setuptools import setup, find_packages

setup(

    name='bfsaas_elk_system', 
    version='0.1', 
    description='BF SAAS System Ch01',
    long_description='BF Software as a Service System Framework for rapid development with device.',
    author='Imran Shaikh',
    platforms='Linux',
    license='Proprietary',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click',
    ],
    entry_points="""
        [console_scripts]
        bfsaas=app_cli.cli:cli
    """,
    
)