from flask import Flask
from flask import render_template
from flask import request

import sys
import logging

from app_library import lib_json as lj
from app_library import lib_logger as logger

from project_services.page import page_blueprint
from project_services.track import track_blueprint
from project_services.health import health_blueprint

from project_services.user import user_blueprint
from project_services.user.models import User
from project_services.admin import admin_blueprint

from project_services.contact import contact


from server_app_extensions import db
from server_app_extensions import login_manager
from server_app_extensions import debug_toolbar
from server_app_extensions import cache
from server_app_extensions import assets_env
from server_app_extensions import rest_api 
from server_app_extensions import celery
from server_app_extensions import apm

from server_app_extensions import csrf


from celery import Celery
from server_app_extensions import mail

from itsdangerous import URLSafeTimedSerializer

app_gen_settings = lj.loadSettings("app_settings/app_general.json")

log_file = True
log_console = True
debug_mode = False

CELERY_TASK_LIST = [
    'project_services.contact.tasks',
]

from flask_mail import Mail, Message



def create_app():

    global log_file,log_console,debug_mode

    app = Flask(__name__)
    app.config.from_object('app_config.config')

    log_file = app.config["LOG_TO_FILE"]
    log_console = app.config["LOG_TO_CONSOLE"]
    debug_mode = app.config["ACTIVATE_DEBUG"]

    app.register_blueprint(page_blueprint)
    app.register_blueprint(track_blueprint)
    app.register_blueprint(health_blueprint)
    app.register_blueprint(user_blueprint)
    app.register_blueprint(admin_blueprint)
    app.register_blueprint(contact)


    extensions(app)
    create_logging(app)

    authentication(app, User)


    return app

def create_logging(app):


    logger.setuplog(log_file,log_console,"app_logs/%Y%m%d_%I%M%S_Audit.log")
    logger.log(log_file,log_console,"Starting {} version {}".format( app_gen_settings['app_name'], app_gen_settings['app_version']))
    logger.log(log_file,log_console,"Server Started.")
    logger.log(log_file,log_console,"In Dev Access Server at > http://localhost:5000/")
    logger.log(log_file,log_console,"In Prod Access Server at > http://dns/")
    logger.log(log_file,log_console,"In Prod Access Track at > http://dns/track")
    logger.log(log_file,log_console,"In Prod Access Result at > http://dns/result")
    logger.log(log_file,log_console,"In Prod Access Health at > http://dns/health")



    return None


def extensions(app):

    debug_toolbar.init_app(app)
    cache.init_app(app)
    
    db.init_app(app)
    login_manager.init_app(app)
    rest_api.init_app(app)
    celery.init_app(app)

    mail.init_app(app)
    csrf.init_app(app)

    apm.init_app(app, server_url='http://localhost:8200', service_name='bfsaas_system', secret_token='changeme', logging=True)

    return None

def authentication(app, user_model):
    """
    Initialize the Flask-Login extension (mutates the app passed in).

    :param app: Flask application instance
    :param user_model: Model that contains the authentication information
    :type user_model: SQLAlchemy model
    :return: None
    """
    login_manager.login_view = 'user.login'

    @login_manager.user_loader
    def load_user(uid):
        return user_model.query.get(uid)

    @login_manager.token_loader
    def load_token(token):
        duration = app.config['REMEMBER_COOKIE_DURATION'].total_seconds()
        serializer = URLSafeTimedSerializer(app.secret_key)

        data = serializer.loads(token, max_age=duration)
        user_uid = data[0]

        return user_model.query.get(user_uid)





def create_celery_app(app=None):
    """
    Create a new Celery object and tie together the Celery config to the app's
    config. Wrap all tasks in the context of the application.

    :param app: Flask app
    :return: Celery app
    """
    app = app or create_app()

    mail.init_app(app)
    db.init_app(app)

    app.register_blueprint(user_blueprint)
    app.register_blueprint(contact)

    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'],include=CELERY_TASK_LIST)
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


# When you directly call the server_app.py file.
if __name__ == "__main__":

    manager = create_app()
    try:
        manager.run(host="localhost",port=5000,debug=debug_mode)
    finally:
        logger.stopmsg(log_file,log_console)