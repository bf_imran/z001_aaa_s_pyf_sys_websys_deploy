#!/bin/bash
set -e
docker-compose -f docker-compose.deploy.yml up -d

echo "Sleeping for 20 seconds for mysql container"
sleep 20  
echo "Resuming script"

docker-compose -f docker-compose.deploy.yml exec bf_system bfsaas db init_db
docker-compose -f docker-compose.deploy.yml exec bf_system bfsaas db init_4u
docker-compose -f docker-compose.deploy.yml exec bf_system service filebeat start
docker-compose -f docker-compose.deploy.yml exec bf_system service metricbeat start
docker-compose -f docker-compose.deploy.yml exec bf_system service apm-server start

docker-compose -f docker-compose.deploy.yml exec bf_worker service filebeat start
docker-compose -f docker-compose.deploy.yml exec bf_worker service metricbeat start
docker-compose -f docker-compose.deploy.yml exec bf_worker service apm-server start

docker-compose -f docker-compose.deploy.yml exec mysql_srv service filebeat start
docker-compose -f docker-compose.deploy.yml exec mysql_srv service metricbeat start
