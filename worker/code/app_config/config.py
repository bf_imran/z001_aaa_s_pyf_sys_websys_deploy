ACTIVATE_DEBUG = False           # In Development best to leave it as True.
LOG_TO_FILE = True               # This will save logs to file defined in app_library\lib_logger
LOG_TO_CONSOLE = True            # This will display logs on console as defined in app_library\lib_logger

DEBUG = False                    # Toggles Debugtoolbar, Show if Tue, Hides if False.

#CACHE_TYPE = 'simple'           # Comment this in developemnt mode, as cache to be off.
ASSETS_DEBUG = True              # Should keep asset caching off in development mode, by setting True here.

MYSQL_DB = True                  # Generate db again if made true, as will add two columns with created time and updated time as string.

#PYTEST PYTEST --COV
#SERVER_NAME = "localhost:5000"  # Only uncomment this when doing pytest, else flask give 404 on all pages.

SECRET_KEY = 'insecurekeyfordev'
SERVER_NAME = 'PLACEHOLDER_VHOST'
#SERVER_NAME = 'localhost'

#SERVER_NAME = 'localhost:5000'

# SQLAlchemy.

#MYSQL Local Wamp
#db_uri = 'mysql://root:abc123@localhost:3308/track_db'

#MYSQL Docker Contianer
db_uri = 'mysql://root:abc123@mysql_srv:3306/track_db'

SQLALCHEMY_DATABASE_URI = db_uri
SQLALCHEMY_TRACK_MODIFICATIONS = False

# User.
SEED_ADMIN_EMAIL = 'root@root.com'
SEED_ADMIN_PASSWORD = 'devpassword'

from datetime import timedelta
REMEMBER_COOKIE_DURATION = timedelta(days=90)

WTF_CSRF_ENABLED = False                        # Toggles off CSRF rquirement i get in Post request.

# Flask-Mail.
MAIL_DEFAULT_SENDER = 'support@gmx.com'
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = False
MAIL_USERNAME = 'bfalcon.emailer@gmail.com'
MAIL_PASSWORD = 'Bf#3mail3r$'
MAIL_TO = 'smimran@gmx.com'

# Celery.
CELERY_BROKER_URL = "redis://redis:6379/0"
CELERY_BACKEND_URL = "redis://redis:6379/0"
CELERY_RESULT_BACKEND = "redis://redis:6379/0" 
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_REDIS_MAX_CONNECTIONS = 5