from flask_debugtoolbar import DebugToolbarExtension
from flask_mail import Mail
from flask_wtf import CsrfProtect

from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_caching import Cache
from flask_assets import Environment
from flask_restful import Api
from flask_celery import Celery
from celery.backends.redis import RedisBackend

from elasticapm.contrib.flask import ElasticAPM

debug_toolbar = DebugToolbarExtension()

mail = Mail()
csrf = CsrfProtect()

db = SQLAlchemy()
login_manager = LoginManager()

cache = Cache()

assets_env = Environment()

rest_api = Api()

celery = Celery()
celery.backend = RedisBackend(app=celery)

apm = ElasticAPM()