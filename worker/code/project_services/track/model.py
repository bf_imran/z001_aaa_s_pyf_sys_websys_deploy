from flask import current_app
from server_app_extensions import db
from app_library.lib_sqlalchemy import ResourceMixin, AwareDateTime

class Track(ResourceMixin,db.Model):

   __tablename__ = 'tracking'

   t_id = db.Column('t_id', db.Integer, primary_key = True)
   t_lat = db.Column('t_lat', db.String(128))
   t_lon = db.Column('t_lon', db.String(128))  

def __init__(self, t_id, t_lat, t_lon):
   self.t_id = t_id
   self.t_lat = t_lat
   self.t_lon = t_lon


#def get_id(self):
#   return self.t_id

#def get_lat(self):
#   return self.t_lat

#def get_lon(self):
#   return self.t_lon




def __init__(self, **kwargs):
   # Call Flask-SQLAlchemy's constructor.
   super(Track, self).__init__(**kwargs)