from server_app import create_celery_app
celery = create_celery_app()


# rF https://docs.celeryproject.org/en/stable/userguide/workers.html
# rF https://stackoverflow.com/questions/25570910/executing-two-tasks-at-the-same-time-with-celery
# celery worker --pool=solo --loglevel=info -A server_app_celery
# celery worker --pool=solo --loglevel=info -A server_app_celery --concurrency=2 -n worker1@%h
# celery worker --pool=solo --loglevel=info -A server_app_celery --concurrency=2 -n worker2@%h
# celery worker --pool=solo --loglevel=info -A server_app_celery -c 2 -n my_worker
# celery worker  --pool=solo --loglevel=info -A server_app_celery -c 2 -n my_worker


# SCRIPTS TO RUN
# -----------------------------------------------------------------

# T1 > python server_app
# T2 > celery worker --pool=solo --loglevel=info -A server_app_celery
# T3 > celery -A server_app_celery beat -l info


# INSTALL FLOWER
# -----------------------------------------------------------------

# pip install flower
# pip install https://github.com/mher/flower/zipball/master#egg=flower

# celery flower -A server_app_celery --address=127.0.0.1 --port=5555