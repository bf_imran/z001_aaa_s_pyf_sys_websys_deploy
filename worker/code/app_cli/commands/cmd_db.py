import click

from sqlalchemy_utils import database_exists, create_database , drop_database
from sqlalchemy import create_engine
from sqlalchemy import inspect

from server_app import create_app
from server_app_extensions import db

from project_services.user.models import User

# Create an app context for the database connection.
app = create_app()
db.app = app


@click.group()
def cli():
    """ Run Database related tasks. """
    pass



@click.command()
def seed_admin():
    """
    Seed the database with an initial user.

    :return: User instance
    """
    if User.find_by_identity(app.config['SEED_ADMIN_EMAIL']) is not None:
        return None

    params = {
        'role': 'admin',
        'email': app.config['SEED_ADMIN_EMAIL'],
        'password': app.config['SEED_ADMIN_PASSWORD']
    }


    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    print ("Seeded the database with an initial Admin user")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')


    return User(**params).save()



@cli.command()
def init_db():
    """Clear database and then create all new tables."""

    db_uri = app.config['SQLALCHEMY_DATABASE_URI']
    if not database_exists(db_uri):
        create_database(db_uri)

    db.drop_all()
    db.create_all()

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    print ("Database Initialized")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')

    return None

@cli.command()
def check_db():
    """Check Db connection."""
    click.echo('Connecting to database and checking tables')

    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    print (engine.table_names())
    click.echo('')
    click.echo('-----------------------')

    return None


@cli.command()
def check_db_schema():
    """Check Db schema."""
    click.echo('Connecting to database and checking table schema')

    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    print (engine.table_names())
    click.echo('')
    click.echo('-----------------------')
    click.echo('')

    inspector = inspect(engine)
    schemas = inspector.get_schema_names()

    for schema in schemas:
        click.echo("schema: %s" % schema)
        for table_name in inspector.get_table_names(schema=schema):
            for column in inspector.get_columns(table_name, schema=schema):
                    click.echo("Column: %s" % column)


    return None




@cli.command()
def seed_2u():
    """
    Seed the database with admina and member.

    :return: User instance
    """
    if User.find_by_identity(app.config['SEED_ADMIN_EMAIL']) is None:

        params = {
            'role': 'admin',
            'email': app.config['SEED_ADMIN_EMAIL'],
            'password': app.config['SEED_ADMIN_PASSWORD'],
            'avatar': 'users/u1.jpg'
        }

        click.echo('')
        click.echo('-----------------------')
        click.echo('')
        click.echo('Admin User Created with id root@root.com/devpassword.')
        click.echo('')
        click.echo('-----------------------')
        click.echo('')

        User(**params).save()

    else :
        
        click.echo('')
        click.echo('-----------------------')
        click.echo('')
        click.echo('Admin User Found, no Admin user created.')
        click.echo('')
        click.echo('-----------------------')
        click.echo('')

    params = {
        'role': 'member',
        'email': 'smimran@gmx.com',
        'password': 'abcd1234',
        'avatar': 'users/u2.jpg'
    }

    User(**params).save()

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    click.echo ("Second User Created with id smimran@gmx.com/abcd1234.")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')


    return None

@cli.command()
def init_2u():
    """
    Seed the database with admina and member.

    :return: User instance
    """
    if User.find_by_identity(app.config['SEED_ADMIN_EMAIL']) is None:

        params = {
            'role': 'admin',
            'email': app.config['SEED_ADMIN_EMAIL'],
            'password': app.config['SEED_ADMIN_PASSWORD'],
            'avatar': 'users/u1.jpg'
        }

        click.echo('')
        click.echo('-----------------------')
        click.echo('')
        click.echo('Admin User Created with id root@root.com/devpassword.')
        click.echo('')
        click.echo('-----------------------')
        click.echo('')

        User(**params).save()

    else :
        
        click.echo('')
        click.echo('-----------------------')
        click.echo('')
        click.echo('Admin User Found, no Admin user created.')
        click.echo('')
        click.echo('-----------------------')
        click.echo('')

    params = {
        'role': 'member',
        'email': 'smimran@gmx.com',
        'password': 'abcd1234',
        'avatar': 'users/u2.jpg'
    }

    User(**params).save()

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    click.echo ("Second User Created with id smimran@gmx.com/abcd1234.")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')


    return None



@cli.command()
def init_4u():
    """
    Seed the database with admina and member.

    :return: User instance
    """
    if User.find_by_identity(app.config['SEED_ADMIN_EMAIL']) is None:

        params = {
            'role': 'admin',
            'email': app.config['SEED_ADMIN_EMAIL'],
            'password': app.config['SEED_ADMIN_PASSWORD'],
            'avatar': 'users/u1.jpg'
        }

        click.echo('')
        click.echo('-----------------------')
        click.echo('')
        click.echo('Admin User Created with id root@root.com/devpassword.')
        click.echo('')
        click.echo('-----------------------')
        click.echo('')

        User(**params).save()

    else :
        
        click.echo('')
        click.echo('-----------------------')
        click.echo('')
        click.echo('Admin User Found, no Admin user created.')
        click.echo('')
        click.echo('-----------------------')
        click.echo('')

    params = {
        'role': 'member',
        'email': 'smimran@gmx.com',
        'password': 'abcd1234',
        'avatar': 'users/u2.jpg'
    }

    User(**params).save()

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    click.echo ("Second User Created with id smimran@gmx.com/abcd1234.")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')

    params = {
        'role': 'member',
        'email': 'device1@device1.com',
        'password': 'def12345',
        'avatar': 'users/u3.jpg'
    }

    User(**params).save()

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    click.echo ("Third User Created with id device1@device1.com/def12345.")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')

    params = {
        'role': 'member',
        'email': 'device2@device2.com',
        'password': 'def12345',
        'avatar': 'users/u4.jpg'
         
    }

    User(**params).save()

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    click.echo ("Third User Created with id device2@device2.com/def12345.")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')

    return None



@cli.command()
def delete_db_tables():
    """Reset the database. deleting all tables."""

    db.drop_all()

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    print ("Database Tables Deleted.")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')

    

    return None

@cli.command()
def delete_db():
    """Delete the database."""

    db_uri = app.config['SQLALCHEMY_DATABASE_URI']
    if database_exists(db_uri):
        drop_database(db_uri)

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    print ("Database Deleted.")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')

    

    return None

@cli.command()
def delete_test_db():
    """Delete test database."""

    db_uri = '{0}_test'.format(app.config['SQLALCHEMY_DATABASE_URI'])

    if database_exists(db_uri):
        drop_database(db_uri)

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    print ("Test Database Deleted.")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')

    

    return None



@cli.command()
def seed_db():
    """
    Seed the database.

    """

    params = {
        't_id': '1',
        't_lat': '24.2424',
        't_lon': '56.5656',
    }


    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    print ("Database seeded.")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')


    Track(**params).save()

    return None



#@click.command()
#@click.option('--with-testdb/--no-with-testdb', default=False,
#              help='Create a test db too?')
#@click.pass_context
#def reset(ctx, with_testdb):
    """
    Init and seed automatically.

    :param with_testdb: Create a test database
    :return: None
    """
#    ctx.invoke(init, with_testdb=with_testdb)
#    ctx.invoke(seed)

#    return None


cli.add_command(seed_admin)
cli.add_command(seed_2u)

cli.add_command(init_db)
cli.add_command(init_2u)
cli.add_command(init_4u)

cli.add_command(delete_db_tables)
cli.add_command(delete_db)
cli.add_command(delete_test_db)

cli.add_command(seed_db)

cli.add_command(check_db)
cli.add_command(check_db_schema)

#cli.add_command(reset)
