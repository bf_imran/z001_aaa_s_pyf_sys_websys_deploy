import json

def loadSettings(settings_file):

    with open(settings_file) as config_file:
        data = json.load(config_file)
        return data